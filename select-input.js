
(function() {
    /**
     * getStyle
     *
     * @param {Element} element   Element to check, and its parents
     * @param {string}  styleProp Style property name
     *
     * @returns {undefined}
     */
    function getStyle(element, styleProp) {
        if (element.currentStyle) {
            return element.currentStyle[styleProp];
        } else if (window.getComputedStyle) {
            return document.defaultView
                .getComputedStyle(element, null)
                .getPropertyValue(styleProp);
        }

        return 'unknown';
    }

    /**
     * Check if the given element or one of its parent have the given style.
     *
     * @param {Element} element    Element to check, and its parents
     * @param {string}  styleProp  Style property name
     * @param {*}       styleValue Style property value
     *
     * @returns {boolean}
     */
    function elementOrParentHasStyle(element, styleProp, styleValue) {
        while (element !== null) {
            if (getStyle(element, styleProp) == styleValue) {
                return true;
            }
            element = element.parentElement;
        }

        return false;
    }

    /**
     * Select the first text input.
     *
     * @returns {undefined}
     */
    function selectFirstTextInput() {
        const inputs = document.querySelectorAll('input[type=search], input[type=text], input[type=password]')

        const to_skip = [
            [ 'display', 'none' ],
            [ 'visibility', 'hidden' ],
            [ 'opacity', 0 ],
            [ '#disabled' ],
        ];

        for (const input of inputs) {
            let skip = false;
            for (var infoToSkip of to_skip) {
                const [ propToSkip, valueToSkip ] = infoToSkip;

                if (propToSkip[0] === '#') {
                    const attribute = propToSkip.substring(1);
                    if (input.hasAttribute(attribute)) {
                        skip = true;
                        break;
                    }
                } else if (elementOrParentHasStyle(input, propToSkip, valueToSkip)) {
                    skip = true;
                    break;
                }
            }
            if (!skip) {
                console.log('Selected input:', input);
                input.select();
                return;
            }
        }
    }


    /********************  MAIN  *******************/

    selectFirstTextInput();

    /***********************************************/
})();
